# Requisitos
- Python 3.6 o superior
- Pip
- Pipenv

# Configurar variables (.env)
- DEVELOPMENT_HOST
- DEVELOPMENT_PORT
- INTEGRATION_HOST
- INTEGRATION_PORT
- PRODUCTION_HOST
- PRODUCTION_PORT

# Ejecutar
- 1. pipenv install
- 2. pipenv run <script>

# Scripts UBUNTU
- dev
- int
- prod

# Scripts WIN
- wdev (testeado en Win10)
- wint (testeado en Win10)
- wprod (testeado en Win10)

# Despliegue
- docker-compose up --build -d  (producción)
- docker-compose -f ./docker.compose.int.yml up --build -d  (integración)